package com.jmp.epam.one.utils;

import java.util.Map;
import java.util.Scanner;

import static java.lang.System.out;

public class UserInterface {

    private static final Scanner SCANNER = new Scanner(System.in);

    static {
        SCANNING = "---------------------------------- SCANNING ......................................... -----------------------------/";
        ENTER_VALID = "Please enter valid action : ";
        INVALID_ACTION = "---------------------------------- YOU HAVE ENTERED INVALID ACTION !!! --------------------------------------------/";
        HI_MAN = "---------------------------------------- HI MAN !!! ---------------------------------------------------------------/";
        EMPTY = "";
        DASHES = "-------------------------------------------------------------------------------------------------------------------/";
        SEARCHING = "---------------------------------- SEARCHING ......................................... ----------------------------/";
        SEARCHING_FINISHED = "---------------------------------- SEARCHING SUCCESSFULLY FINISHED !!! --------------------------------------------/";
        SCAN_FINISHED = "---------------------------------- SCAN SUCCESSFULLY FINISHED !!! -------------------------------------------------/";
        BYE_BYE = "------------------------------------------- BYE BYE !!! -----------------------------------------------------------/";
        ENTER_FILENAME = "Please enter file name you want to find : ";
        NOTHING_TO_SEARCH = "NO DATA TO SEARCH, FIRSTLY SEARCHING YOU SHOULD SCAN !!!";
        NOTHING_WERE_FOUND = "NOTHING WERE FOUND !!!";
        ENTER_CHOICE = "Please enter your choice : ";
    }

    private static final String DASHES;
    private static final String EMPTY;
    private static final String HI_MAN;
    private static final String INVALID_ACTION;
    private static final String ENTER_VALID;
    private static final String SCANNING;
    private static final String SEARCHING;
    private static final String SCAN_FINISHED;
    private static final String SEARCHING_FINISHED;
    private static final String BYE_BYE;
    private static final String ENTER_FILENAME;
    private static final String NOTHING_TO_SEARCH;
    private static final String NOTHING_WERE_FOUND;
    private static final String ENTER_CHOICE;

    public static void greet() {
        superPrint(HI_MAN);
        out.println(EMPTY);
    }

    public static String invalidUserInput() {
        superPrint(INVALID_ACTION);
        out.print(ENTER_VALID);

        return SCANNER.next().toUpperCase();
    }

    public static void scan() {
        superPrint(SCANNING);
    }


    public static void search() {
        superPrint(SEARCHING);
    }

    public static void notifyAboutFinishingScanning() {
        superPrint(SCAN_FINISHED);
    }

    public static void notifyAboutFinishingSearching() {
        superPrint(SEARCHING_FINISHED);
    }

    public static void exit() {
        out.println(DASHES);
        superPrint(BYE_BYE);
    }

    public static void printResult(Map<String, String> searchResults) {
        for (Map.Entry<String, String> stringStringEntry : searchResults.entrySet()) {
            out.println(stringStringEntry);
        }
    }

    public static String getUserInputToSearch() {
        out.print(ENTER_FILENAME);

        return SCANNER.next();
    }

    public static String waitingForUserEnter() {
        out.print(ENTER_CHOICE);

        return SCANNER.next().toUpperCase();
    }

    private static void superPrint(String Message) {
        out.println(DASHES);
        out.println(DASHES);
        out.println(Message);
        out.println(DASHES);
        out.println(DASHES);
    }

    public static void nothingFound() {
        out.println(NOTHING_WERE_FOUND);
    }

    public static void invalidSearch() {
        out.println(NOTHING_TO_SEARCH);
    }
}
